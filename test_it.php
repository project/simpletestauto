<?php
if ($argc != 2) {
  die("Proper usage is test_it.php nid");
}

$test_it_path = realpath(dirname(__FILE__));

chdir(realpath(dirname(__FILE__) . '/../../'));
if (file_exists('./includes/bootstrap.inc')) {
  include_once './includes/bootstrap.inc';
}
else {
  chdir(getcwd() . '/../../');
  if (file_exists('./includes/bootstrap.inc')) {
    include_once './includes/bootstrap.inc';
  }
  else {
  	die("bootstrap.inc could not be loaded");
  }
}
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

$node_id = $argv[1];

$drupal_dir = getcwd();

$node = node_load($node_id);
$patch_url = $node->patch_url;
$project_name = $node->project;
$version = $node->version;
$branch = getBranchName($version);
$msg = "";
$db_host = 'localhost';
$db_user = 'db_user';
$db_pass = 'db_pass';
$install_dir = variable_get('simpletestauto_install_dir', '');
$test_server_url = variable_get('simpletestauto_test_url', '');
$simpletest_lib_cvs = '-d:pserver:anonymous@simpletest.cvs.sourceforge.net:/cvsroot/simpletest';

$simpletest_modules = preg_split("/[\s,]+/", variable_get('simpletestauto_test_modules','').', simpletest');
$default_modules = array('block', 'comment', 'filter', 'help', 'menu', 'node', 'system', 'taxonomy', 'user', 'watchdog');

// Generate a unique instance name.
$instance = "test_" . time() . rand(0, 32767);
$instance_dir = $install_dir."/".$instance;

chdir($install_dir) || terminateTesting(1, $msg."Unable to chdir to $install_dir");
mkdir($instance_dir) || terminateTesting(1, $msg."Unable to mkdir $instance_dir\n");
mkdir($instance_dir.'/files') || terminateTesting(1, $msg."Unable to crate files dir\n");

$forbidden_dirs = array('.', '..', 'CVS');
if ($project_name == "Drupal") {
  $forbidden_dirs[] = 'modules';
  $patch_dir = $instance_dir;
} else {
  $patch_dir = $instance_dir.'/modules/'.$project_name;
  $simpletest_modules[] = $project_name;
}
$profile_modules = array_merge($default_modules, $simpletest_modules);

//Check out drupal core from cvs
$exec_cmd = 'cvs -d:pserver:anonymous:anonymous@cvs.drupal.org:/cvs/drupal co -d' . $instance.' -r ' . $branch . ' drupal';
exec($exec_cmd, $output, $status);
if ($status) {
  terminateTesting(1, $msg."Unable to check Drupal branch $branch out of CVS.");
}

chdir($instance_dir) || terminateTesting(1, $msg."Unable to chdir to $instance_dir");

//Check out needed modules
foreach($simpletest_modules as $module) {
  $exec_cmd = 'cvs -d:pserver:anonymous:anonymous@cvs.drupal.org:/cvs/drupal-contrib co -d modules/'.$module.' -r ' . $branch . ' contributions/modules/'.$module;
  print($exec_cmd);
  exec($exec_cmd, $output, $status);
  if ($status) {
    terminateTesting(1, $msg."Unable to check $module module branch $branch out of CVS. CMD: $exec_cmd");
  }
}

chdir("modules/simpletest") || terminateTesting(1, $msg."Unable to chdir to modules/simpletest");

//delete some tests until they are fixed
unlink('tests/upload_tests.test');
unlink('tests/taxonomy.module.test');

// Check out simpletest library from their cvs
$exec_cmd = 'cvs -z3 '. $simpletest_lib_cvs .' co -d simpletest -P simpletest';
exec($exec_cmd, $output, $status);
if ($status) {
  terminateTesting(1, $msg."Unable to check out simpletest libraries from CVS.");
}

chdir($instance_dir) || terminateTesting(1, $msg."Unable to chdir to $instance_dir");

//creating database
$exec_cmd = "mysqladmin -u $db_user -p$db_pass create $instance";
exec($exec_cmd, $output, $status);
if ($status) {
  terminateTesting(1, $msg."Unable to create database $instance.");
}
print($exec_cmd);
//changing sed output to same file ends up with empty file thats why /tmp is used
$exec_cmd = 'sed "s/' . 
  preg_quote('mysql://username:password@localhost/databasename', '/') . '/' . 
  preg_quote('mysql://' . $db_user . ':' . $db_pass . '@localhost/' . $instance, '/' ) . '/; /' .
  '# \$base_url/a\\' .
  '$base_url = \''.
  preg_quote($test_server_url . $instance, '/') . '\';" ' .
  'sites/default/settings.php > /tmp/'.$instance.'settings.php; mv /tmp/'.$instance.'settings.php sites/default/settings.php';

exec($exec_cmd, $output, $status);
print($exec_cmd);
if ($status) {
  terminateTesting(1, $msg."Unable to set this instance settings.");
}

$settings_file_content = file_get_contents('sites/default/settings.php');
if (!strstr($settings_file_content, $instance)) {
  terminateTesting(1, $msg."Settings file was not modified correctly.");
}

installDrupal($version);

//inserting admin user
$exec_cmd = "mysql -u $db_user -p$db_pass $instance < $test_it_path/additions.mysql";
exec($exec_cmd, $output, $status);
if ($status) {
  terminateTesting(1, $msg."Unable to insert additions into database.");
}



//applying patch
$exec_cmd = 'wget -O - '.$patch_url.' | patch -p0 ';
exec($exec_cmd, $outputPatch, $status);
if ($status) {
  terminateTesting(1, "Unable to patch file.<br>" . implode("<br>", $outputPatch));
}

$test_result_file = '/tmp/'.$instance.'result.txt';
$exec_cmd = 'php modules/simpletest/run_all_tests.php > '.$test_result_file;

exec($exec_cmd, $output, $status);

$results = file_get_contents($test_result_file);
$msg .= $results;

if (!unlink($test_result_file)) {
	terminateTesting(1, $msg."Failed to delete test result file");
}

terminateTesting(0, $msg);

/**
 * Function that sends response to test server and calls exit
 * @param return_address address of the server we would like to send results to
 * @param node_id whihc node id is this response for
 * @param exit_status arguemnt for exit call
 * @param msg message string for sending to test server 
 */
function terminateTesting($exit_status, $msg) {
  global $drupal_dir, $return_address, $node_id, $output, $executed_commands, $debug;
  $status = -1;
  if ($exit_status == 0) {
    $result = explode('\n', $msg);
    $stats = $result[count($result)-1];
    $stats = explode(', ', $stats);
    $passes = 0;
    $failes = 0;
    foreach($stats as $stat) {
    $stat = explode(': ', $stat);
    switch ($stat[0]) {
      case 'Passes':
        $passes += $stat[1];
        break;
      case 'Failures':
      case 'Exceptions':
        $fails += $stat[1];
        break;
      }
    }
    if ($fails == 0 && $passes != 0) {
      $status = 0;
    }
    else {
      $status = 1;
    }
  }
  if ($status == -1) {
    $status == $exit_status;
  }
  fopen('.clean','w');
  chdir($drupal_dir);
  if ($debug) {
    $commands = implode("<br>", $executed_commands);
    $out = implode("<br>", $output);
    $msg .= "Executed commands:<br>" . $commands . "<br>";
    $msg .= "Output:<br>" . $out;
  }
  simpletestauto_writeResult($node_id, $msg, $status);
  exit($exit_status);
}

/**
 * Installs drupal 
 */
function installDrupal($version) {
  global $test_server_url, $instance, $profile_modules, $simpletest_modules, $db_user, $db_pass;
  $output = NULL;
  $status = NULL;
  switch ($version) {
    case 'HEAD':
      enableTestModules($profile_modules, $version);
      $exec_cmd = 'wget -O - '.$test_server_url.$instance.'/install.php?profile=test';
      exec($exec_cmd, $output, $status);
      if ($status) {
        terminateTesting(1, "Unable to install drupal.");
      }
      break;
    case '4.7':
      $exec_cmd = "mysql -u $db_user -p$db_pass $instance < database/database.4.1.mysql";
      exec($exec_cmd, $output, $status);
      if ($status) {
        terminateTesting(1, "Unable to insert tables into database.");
      }
      enableTestModules($simpletest_modules, $version);
      break;
  }
}

/**
 * Returns a drupal branch name for that $version
 * @param $version version of the branch
 */
function getBranchName($version) {
  if ($version == 'HEAD') {
  	return 'HEAD';
  }
  return 'DRUPAL-'.str_replace(".", "-", $version);
}

/**
 * creates a new profile file so that this modules 
 * are enabled at install time in drupal higher than 4.7
 * @param $profile_modules array of modules that will be enabled
 */
function enableTestModules($modules, $version) {
  global $instance_dir, $instance, $db_user, $db_pass;
  $output = "";
  $status = "";
  chdir($instance_dir) || terminateTesting(1, "Unable to chdir to $instance_dir");
  switch ($version) {
    case 'HEAD':
      //create test profile file so that all additional modules are enabled
      $output = "<?php\nfunction test_profile_modules() {\n return array(";
      foreach ($modules as $module) {
        $output .= "\n'".$module."',";
      }
      $output .= "\n);\n}\n?>";
      mkdir('profiles/test') || terminateTesting(1, "Unable to mkdir $instance_dir/profiles/test\n");
      $handle = fopen("profiles/test/test.profile", "w");
      if (fwrite($handle, $output) === FALSE) {
        terminateTesting(1, "Could not write to test.profile.");
      }
      fclose($handle);
      break;
    case '4.7':
      $output = "";
      foreach ($modules as $module) {
      	$output .= "INSERT INTO system VALUES ('modules/$module/$module.module', '$module', 'module', '$module', 1, 0, 0, 0, 0);\n";
      }
      $handle = fopen("/tmp/$instance.sql", "w");
      if (fwrite($handle, $output) === FALSE) {
        terminateTesting(1, "Could not write additions to tmp file.");
      }
      fclose($handle);
      $exec_cmd = "mysql -u $db_user -p$db_pass $instance < /tmp/$instance.sql";
      exec($exec_cmd, $output, $status);
      if ($status) {
        terminateTesting(1, "Unable to execute $exec_cmd.");
      }
      break;
  }
}
?>

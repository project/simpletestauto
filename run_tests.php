<?php
chdir(realpath(dirname(__FILE__) . '/../../'));
if (file_exists('./includes/bootstrap.inc')) {
  include_once './includes/bootstrap.inc';
}
else {
  chdir(getcwd() . '/../../');
  if (file_exists('./includes/bootstrap.inc')) {
    include_once './includes/bootstrap.inc';
  }
  else {
  	die("bootstrap.inc could not be loaded");
  }
}
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
global $base_url;

$nodes = get_untested_patches();
foreach( $nodes as $nid) {
  $node = node_load($nid);
  $node->body .= "<br>".format_date(time(), 'large')."<br>Test started.";
  $node->tested = 1;
  node_save($node);
  $test_it_path = realpath(dirname(__FILE__));
  exec("php $test_it_path/test_it.php $node->nid > /dev/null &");
}

?>

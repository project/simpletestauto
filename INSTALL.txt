Simpletest automation installation instrucions

Prerequisites:
- two installations of drupal 4.7 (can be on same server), one is used
to get patches (simpletestauto block or from issue tracker) and then
sends them to other installation.The other one is used to receive
patches and put them in test queue and then run test each one
- a directory on the server that is writable by apache and accessible
by apache from the web. It can be secured to only be accessed from
localhost.
- database user that has permission that are needed by drupal and to
create and drop database


First server
1. install module (please use the latest version from cvs) and enable it
2. on settings page enter the testing server password, choose yes in
patch server settings-> is this server used to catch patches, in
automatic server url write url of xmlrpc.php file of the other
installation
3. enable test your patch block

Second server
1. install module (please use the latest version from cvs) and enable it
2. on the settings page enter the same testing server password as on
the first server, choose yes test server settings->is this server used
as testing server
3. write in additional modules (image is needed), install dir is
directory in which test environment will be created and test server
url is url of this directory
4. in simpletestauto module directory change .htaccess file so it
reflect correct database user settings
5. setup cron to run drupal cron.php which is used to delete old tests
and run_tests.php in simpletestauto directory ( run_tests.php is used
to run untested tests). Sth like 00 * * * * php /path/to/your/drupal/dir/modules/simpletest/run_tests.php

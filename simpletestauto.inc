<?php
function simpletestauto_test_insert($node) {
  db_query("INSERT INTO {simpletestauto_test} (nid, project, patch_url, version, tested) VALUES (%d, '%s', '%s', '%s', '%d')", $node->nid, $node->project, $node->patch_url,$node->version, $node->tested);
}

function simpletestauto_test_update($node) {
  db_query("UPDATE {simpletestauto_test} SET project = '%s', patch_url = '%s', version = '%s', tested = '%d' WHERE nid = %d", $node->project, $node->patch_url, $node->version, $node->tested, $node->nid);
}

function simpletestauto_test_load($node) {
  $test = db_fetch_object(db_query('SELECT * FROM {simpletestauto_test} WHERE nid = %d', $node->nid));
  return $test;
}

function simpletestauto_test_delete($node) {
  db_query('DELETE FROM {simpletestauto_test} WHERE nid = %d', $node->nid);
}

function simpletestauto_test_form(&$node) {
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#required' => TRUE,
    '#default_value' => $node->title,
    '#weight' => -5
  );
 $form['body_filter']['body'] = array(
    '#type' => 'textarea',
    '#title' => t('Body'),
    '#default_value' => $node->body,
    '#required' => FALSE
  );
  $form['body_filter']['filter'] = filter_form($node->format);

  $form['project'] = array(
    '#type' => 'textfield',
    '#title' => t('Test project'),
    '#default_value' => $node->project,
  );
  $form['patch_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Patch url'),
    '#default_value' => $node->patch_url,
  );
  $form['version'] = array(
    '#type' => 'textfield',
    '#title' => t('Project version'),
    '#default_value' => $node->version,
  );
  
  return $form;
}

/**
 * Implementation of hook_content().
 */
function simpletestauto_test_content($node, $teaser = false) {
  return node_prepare($node, $teaser);
}

/**
 * Retunrs all patches that are not tested
 * @return array of node ids of untested patches
 */
function get_untested_patches() {
  $result = db_query("SELECT nid FROM {simpletestauto_test} WHERE tested = 0");
  $nodes = array();
  while($patch = db_fetch_object($result)) {
  	$nodes[] = $patch->nid;
  }
  return $nodes;
}

function simpletestauto_test_view(&$node, $teaser = false, $page = false) {

  if (!$teaser && $page) {
    $node = node_prepare($node, $teaser);
    switch ($node->tested) {
      case SIMPLETESTAUTO_PASS:
        $status = t('PASSED');
        break;
      case SIMPLETESTAUTO_FAIL:
        $status = t('FAILED');
        break;
      case SIMPLETESTAUTO_NOT_TESTED:
        $status = t('NOT TESTED');
        break;
      case SIMPLETESTAUTO_SUBMITED:
        $status = t('SUBMITED TO TEST SERVER');
        break;
      case SIMPLETESTAUTO_IN_PROGRESS:
        $status = t('TESTING IN PROGRESS');
        break;    
    }
    $node->content['summary'] = array (
      '#value' => theme('simpletestauto_summary',$node->project, $node->version, $node->patch_url, $status),
    );
  }
  return $node;
}

function theme_simpletestauto_summary($project, $version, $patch_url, $status) {
  
  $rows = array();
  $rows[] = array('Project:', check_plain($project));
  $rows[] = array('Version:', check_plain($version));
  $rows[] = array(t('Patch url:'),  l($patch_url, $patch_url));
  $rows[] = array(t('Status:'), check_plain($status));
  $output .= '<div class="summary">'. theme('table', array(), $rows) .'</div>';
  return $output;
}

?>
